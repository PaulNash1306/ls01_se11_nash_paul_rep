import java.util.Scanner;
public class AB_Verzweigungen_1_A2 {

	// 2. Aufgabe
	// Der Benutzer gibt zwei reelle Zahlen ein. Falls die Zahlen gleich gro� sind, werden sie
	// nebeneinander ausgegeben, sonst untereinander, die kleinere zuerst. 
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		double a; 
		double b; 
		
		System.out.println("Geben Sie zwei reelle Zahlen ein");
		a = sc.nextDouble();
		b = sc.nextDouble();
	
		
		if(a==b) 
		{
			System.out.printf( "%.2f %.2f"  ,a , b  );
		}
		else 
		{
			if(a<b)
			{		
				System.out.printf("%.2f\n%.2f"   , a, b);
			}
			else
			{
				System.out.printf("%.2f\n%.2f", b, a);
		
			}
				
		}
		
	}

}
