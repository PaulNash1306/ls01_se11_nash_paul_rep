import java.util.Scanner;
public class AB_Verzweigungen_1_A1 {
	
//1. Aufgabe Der Benutzer gibt 2 ganzzahlige Werte ein. Wenn die Eingaben gleich sind, dann Ausgabe �Gleiche Werte�.

	public static void main(String[] args) {
		
			Scanner sc = new Scanner(System.in);
			int a; int b;
	
		System.out.println("Geben Sie 2 Ganzzahlen ein");
		a = sc.nextInt();
		b = sc.nextInt();
		
		if (a==b)
		{
			System.out.println("Gleicher Wert");
		}
		else 
		{
			System.out.println("Ungleicher Wert");
		}
		sc.close();
	}
	
}
