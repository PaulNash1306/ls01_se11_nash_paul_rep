import java.util.Scanner;
public class AB_Verzweigungen_1_A4 {
	
	//4. Aufgabe
	//Der Benutzer soll eine Zahl zwischen 0 und 100 eingeben (einschlie�lich der Grenzen). Eine
	//Zahl au�erhalb des Bereichs wird mit �Fehlerhafte Eingabe� quittiert.
	//Eine Zahl >50 mit �groߓ, sonst �klein�. 

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int x;
		System.out.println("Geben Sie eine Zahl zwischen 0 und 100 ein.");
		x = sc.nextInt();
	
		if(x<0){
			System.out.println("Fehlerhafte Eingabe");
		}
		else {
			if(x>100){
				System.out.println( "Fehlerhafte Eingabe");
			}
			else {
				if(x>=50){
					System.out.println("Gro� " + x);
				}
				else {
					if(x>=0) {
						System.out.println("Klein " + x);
					}
				}
			}
		}
		
		sc.close();
		
	}

}
