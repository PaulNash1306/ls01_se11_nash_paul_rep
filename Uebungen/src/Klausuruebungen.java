
//Switch case mit allen SChleifen

	import java.util.Scanner;

	public class Klausuruebungen {

	  public static void main(String[] args) {
	    Scanner sc = new Scanner(System.in);
	    short i = 1;
	    String wahl;
	   
	    System.out.println("Möchten Sie Schleife while / for / do-while ausführen lassen?");
	    wahl = sc.next();
	    switch (wahl) {
	      case "while" :
	        System.out.println("While Schleife:");  
	        //Schleife 1
	        while (i<10) {
	          System.out.println(i);
	          i++;
	          
	        } // end of while
	        break;
	        
	      case "for" :
	        System.out.println("for Schleife:");
	        //Schleife 2
	        for (short n=1;n<10 ;n++ ) {
	          System.out.println(n);
	          
	        } // end of for
	        
	        break;
	      case "do-while" :
	        System.out.println("do-While Schleife:");
	        //Schleife 3
	        short m = 1;
	        System.out.println("");
	        
	        do {
	          System.out.println(m);
	          m++;
	          
	        } while (m<10); // end of do-while
	        
	        
	        
	        break;  
	      default:  System.out.println("Falsche Eingabe");
	        
	    } // end of switch
	    
	    
	    System.out.println("");
	    
	    
	    
	    
	    
	    
	    
	    
	  }
	}