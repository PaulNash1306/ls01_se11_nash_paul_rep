
import java.util.Scanner;
public class arrayEinfuehrung {
  
  public static void main(String[] args) {
   
    Scanner sc = new Scanner (System.in);
    
    //Deklaration eines Arrays, 5 integer Werte
    //Name des Arrays: "intArray"
    
    int [] intArray = new int[5];
    
    
    
    //Speichern von Werten im Array
    //Wert 1000 an 3. Stelle des Arrays
    //Wert 500 an Index 4
    
    intArray [2] = 1000;
    intArray [4] = 500;
    
    
    //Deklaration + Initialisierung eines Arrays in einem Schritt
    //Name des Arrays: "doubleArray"
    // L�nge 3, mit 3 double Werten 
    
    double [] doubleArray = {1.0, 2.0, 3.0};
    
    //Der Wert des double Arrays an Indexposition 1 soll ausgegeben werden
    
    System.out.println(doubleArray[1]);
    
    //Nutzer soll einen Index angeben 
    //und an diesen Index einen neuen Wert speichern
    System.out.println("An welchen Index soll der neue Wert?");
    
    
    int Index = sc.nextInt();
    
    System.out.println("Geben Sie den neuen Wert f�r index " +  Index    + " an:");
    
    doubleArray [Index] =sc.nextDouble(); 
    
    
    
    
    
    
    
    //Alle Werte vom Array intArray sollen ausgegeben werden
    //Geben Sie zun�chst an, welche Werte Sie in der Ausgabe erwarten: 
    // _0_   _0_  _1000_  _0_  _500_  
    
    for (Index = 0; Index <5 ; Index++) {
    		System.out.println(intArray[Index]);
    }//end of for
    
    
    //Der intArray soll mit neuen Werten gef�llt werden
          //1. alle Felder sollen den Wert 0 erhalten
    
    System.out.println(Index);
    for (Index =0; Index <5; Index++) {
    	intArray [Index] = 0;
    	System.out.println(intArray [Index]);
    }
    
    
    
    //Der intArray soll mit neuen Werten gef�llt werden
          //2. alle Felder sollen vom Nutzer einen Wert bekommen
          //nutzen Sie dieses Mal die Funktion �length�
    
    for (Index =0 ; Index <intArray.length; Index++) {
    
    	System.out.println(intArray [Index]);
    	intArray [Index] = sc.nextInt();
    	System.out.println(intArray [Index]);
    }
    
    //Der intArray soll mit neuen Werten gef�ll�t werden
           //3. Felder automatisch mit folgenden Zahlen f�llen: 10,20,30,40,50
    
    
    
    
    
    
  } // end of main
  
} // end of class arrayEinfuehrung
