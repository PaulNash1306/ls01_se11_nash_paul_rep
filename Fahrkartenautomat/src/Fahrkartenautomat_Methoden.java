import java.util.Scanner;

class Fahrkartenautomat_Methoden
{
    public static void main(String[] args)
    { 	
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag = 0;
    	double eingezahlterGesamtbetrag;
    	
    	zuZahlenderBetrag = ticketpreis(0,0);
    	eingezahlterGesamtbetrag =	geldeinwurf(0,zuZahlenderBetrag, 0);
    	fahrscheinausgabe();
    	rueckgeld(0,eingezahlterGesamtbetrag,zuZahlenderBetrag);
  

    	
    
    		
    }
    //Ticketanzahl und Preis
    public static double ticketpreis(double zuZahlenderBetrag, double anzahlfahrkarten){ 
    	Scanner tastatur = new Scanner(System.in);
    	System.out.println("Wie viele Tickets m�chten sie kaufen? (mind.-max. Kaufmenge = 1-10");
    	anzahlfahrkarten = tastatur.nextDouble();
    	
    	if(anzahlfahrkarten>10) {
    		System.out.println("Kaufmenge zu hoch. Es wird eine Karte ausgegeben");
    		
    		System.out.println("Ticketpreis (EURO): ");
	        zuZahlenderBetrag = tastatur.nextDouble();
	        zuZahlenderBetrag *= anzahlfahrkarten = 1;
    	}
    	else {
    		if(anzahlfahrkarten<1){
    			System.out.println("Ung�ltige Eingabe. Es wird eine Karte ausgegeben");
    			
    			System.out.println("Ticketpreis (EURO): ");
    	        zuZahlenderBetrag = tastatur.nextDouble();
    	        zuZahlenderBetrag *= anzahlfahrkarten = 1;
    		}
    		else {
    		
    			System.out.println("Ticketpreis (EURO): ");
    	        zuZahlenderBetrag = tastatur.nextDouble();
    	        zuZahlenderBetrag *= anzahlfahrkarten;
    	       
    		} 
    	}
    	
    	 return zuZahlenderBetrag;
    	 
        
    }
    //geldeinwurf
    public static double geldeinwurf(double eingezahlterGesamtbetrag, double zuZahlenderBetrag, double eingeworfeneM�nze){
    	 eingezahlterGesamtbetrag = 0.0;
    	 Scanner tastatur = new Scanner(System.in);
         while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
         {
      	   System.out.printf("Noch zu zahlen: %.2f" ,  + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
      	   System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
      	   eingeworfeneM�nze = tastatur.nextDouble();
             eingezahlterGesamtbetrag += eingeworfeneM�nze;
         }
		return eingezahlterGesamtbetrag;  
		}
    
    //fahrscheinausgabe
    public static void fahrscheinausgabe() {
    	  System.out.println("\nFahrschein(e) wird ausgegeben");
          for (int i = 0; i < 8; i++)
          {
             System.out.print("=");
             try {
   			Thread.sleep(250);
   		} catch (InterruptedException e) {
   			// TODO Auto-generated catch block
   			e.printStackTrace();
   		}
          }
          System.out.println("\n\n");
        }
   
    //r�ckgeldberechnung und -ausgabe hier nimmt der
    public static void rueckgeld(double r�ckgabebetrag, double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	 r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
         if(r�ckgabebetrag > 0.0)
         {
      	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f  EURO " , r�ckgabebetrag);
      	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

             while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
             {
          	  System.out.println("2 EURO");
  	          r�ckgabebetrag -= 2.0;
             }
             while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
             {
          	  System.out.println("1 EURO");
  	          r�ckgabebetrag -= 1.0;
             }
             while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
             {
          	  System.out.println("50 CENT");
  	          r�ckgabebetrag -= 0.5;
             }
             while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
             {
          	  System.out.println("20 CENT");
   	          r�ckgabebetrag -= 0.2;
             }
             while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
             {
          	  System.out.println("10 CENT");
  	          r�ckgabebetrag -= 0.1;
             }
             while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
             {
          	  System.out.println("5 CENT");
   	          r�ckgabebetrag -= 0.05;
             }
         }

         System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                            "vor Fahrtantritt entwerten zu lassen!\n"+
                            "Wir w�nschen Ihnen eine gute Fahrt.");
      }
    }