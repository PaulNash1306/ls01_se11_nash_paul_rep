import java.util.Scanner;

class uebung_addition { 

	//Methodenbezeichner/Methodenname
    public static void main(String[] args) {  //Parameter //Ganze obere Line ist der Methodenkopf
    	//Zugriffsmodifizierer
    	 Scanner sc = new Scanner(System.in);
        double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;	//Methodenrumpf ist alles zwischen den Methoden-Klammern
        
        programmhinweis(); //Methodenaufruf
       
       zahl1 = eingabe(1);
       zahl2 = eingabe(2);

       	
       erg = verarbeitung(erg, zahl1, zahl2); //Argument
        
        ausgabe(zahl1, zahl2, erg);
        sc.close();
   
    } //Methodendefinition - ganze Methode
 
   public static void programmhinweis() {  //Methodenkopf 
    	System.out.println("Hinweis: ");
    	System.out.println("Das Programm addiert 2 eingegebene Zahlen. "); 
    } //Methodenk�rper
    
  public static void ausgabe(double zahl1, double zahl2, double erg  ) {
	 System.out.println("Ergebnis der Addition");
     System.out.printf("%.2f = %.2f + %.2f", erg, zahl1, zahl2); 
    }		//R�ckgabedatentyp					
  
  public static double verarbeitung(double erg, double zahl1, double zahl2 ) {
    	erg=zahl1+zahl2;
    	return erg;
    }	//R�ckgabewert 		//Lokale Variable Ich check die Reihenfolge der bzw den Aufbau der Scanner Methode nicht. 
  
  public static double eingabe(int z) {
    	Scanner sc = new Scanner(System.in);
   	 double zahl1;
   	
    	System.out.println(z + ". Zahl");
        zahl1 = sc.nextDouble();
        
        return zahl1;	
  
      
        
    }
}