import java.util.Scanner;

public class Kontostand {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		float kontostand = 1000;
		float betrag;
		
		
		System.out.println("Guten Tag, wie viel Geld m�chten Sie einzahlen?");
		betrag = tastatur.nextFloat();
		kontostand = einzahlen(betrag, kontostand); //
		System.out.println("Kontostand: " + kontostand);
		
		System.out.println("Wie viel m�chten Sie abheben?");
		betrag = tastatur.nextFloat();
		kontostand = abheben(betrag, kontostand);
		System.out.println("Kontostand: " + kontostand);
		

	
	}
	//Methoden zum Einzahlen
	
	public static float einzahlen (float betrag, float kontostand) {
		kontostand = kontostand + betrag;
		return kontostand;
		
	}
	//methode zum Abheben
	public static float abheben (float betrag, float kontostand) {
		kontostand = kontostand - betrag;
		return kontostand;
	}
			
}
